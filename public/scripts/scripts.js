/*eslint-env jquery, browser*/
function onUploadClick() {

    $("#alertDanger").remove();
    $("#alertSuccess").remove();
    $(".jsoncontainer").remove();
    $("#jjson").remove();


 	$(".btn-danger").addClass("hidden");
    $("#spinnerlink").removeClass("hidden");
    $("#spinnerlink").addClass("glyphicon glyphicon-refresh glyphicon-refresh-animate");
   // $("#uploadButton").removeClass("glyphicon-globe");
   // $("#uploadButton").addClass("glyphicon-refresh glyphicon-refresh-animate");
   
   
  	//check wheather the function is called from the recommendations tab
	if (arguments[0] == "recommendation") {
    	var input = $("#inputCryptoURL").val();
    	var inputUrl = "https://canvanizer.com/canvas/" + input;
	}else {
		var input = $("#basic-url").val();
    	var inputUrl = "https://canvanizer.com/canvas/" + input;
		}

    if (input != "") {

        $.ajax({
            type: 'POST',
            dataType: 'json',
    		contentType: 'application/json',
        	data: JSON.stringify( { "url": inputUrl } ),
            url: '/getCanvas',
            
            success: function(data) {

                var alertHTML = '<div id="alertSuccess" style="display: none;" class="alert alert-success alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Upload successful.</strong> Canvas was added to the Database.' +
                    '</div>';
                    
                if (arguments[0] == "recommendation") {
    				$("#recommendation").append(alertHTML);
                	$("#alertSuccess").show();
                	$("#recommendation").append('<div class="jsoncontainer"><div id="jjson" class="jjson"></div></div>');
				}else {
					$("#link").append(alertHTML);
                	$("#alertSuccess").show();
                	$("#link").append('<div class="jsoncontainer"><div id="jjson" class="jjson"></div></div>');
				}
                
                $("#jjson").jJsonViewer(JSON.stringify(data.values), {
                    expanded: true
                });


                $(".btn-danger").removeClass("hidden");
		        $("#spinnerlink").addClass("hidden");
		        $("#spinnerlink").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");


            },
            error: function(data) {

                var message = data.responseText;
                var alertHTML = '<div id="alertDanger" style="display: none;" class="alert alert-danger alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Error in Processing: </strong>' +
                    '</div>';
                
                
				if (arguments[0] == "recommendation") {
    				$("#recommendation").append(alertHTML);
				}else {
					$("#link").append(alertHTML);
				}


                $("#alertDanger").append(message);
                $("#alertDanger").show();
				$(".btn-danger").removeClass("hidden");
		        $("#spinnerlink").addClass("hidden");
		        $("#spinnerlink").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");
            }

        });
    } else {
        var alertHTML = '<div id="alertDanger" style="display: none;" class="alert alert-danger alert-dismissible" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '</div>';
        
        if (arguments[0] == "recommendation") {
    		$("#recommendation").append(alertHTML);
		}else {
			$("#link").append(alertHTML);
		}
		
        $("#alertDanger").append("<strong>Empty URL:</strong> Please enter your Canvas ID");
        $("#alertDanger").show();


		$(".btn-danger").removeClass("hidden");
        $("#spinner").addClass("hidden");
        $("#spinner").removeClass("glyphicon-refresh glyphicon-refresh-animate");
        //$(".btn-primary").removeClass("disabled");
        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
        //$("#uploadButton").addClass("glyphicon-globe");

    }

}

function onCrawlClick(extract) {

    $("#alertSuccess").remove();
    $("#alertDanger").remove();
    $("#urls").remove();
    $(".jsoncontainer").remove();
    $("#jjson").remove();
    $("#resultCounts").remove();
    

    $(".btn-danger").addClass("hidden");
    $("#spinnercrawl").removeClass("hidden");
    $("#spinnercrawl").addClass("glyphicon glyphicon-refresh glyphicon-refresh-animate");
    //$("#crawlTrue").removeClass("glyphicon-scissors");
    //$("#crawlFalse").removeClass("glyphicon-search");
    //$("#crawlFalse").addClass("glyphicon-refresh glyphicon-refresh-animate");
    //$("#crawlTrue").addClass("glyphicon-refresh glyphicon-refresh-animate");

    $.ajax({
        type: 'POST',
        url: '/crawl',
        timeout: 100000,
        dataType: 'json',
    	contentType: 'application/json',
        data: JSON.stringify( { "extract": extract } ),
        success: function(data) {

          
            var alertHTML = '<div id="alertSuccess" style="display: none;" class="alert alert-success alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '<strong>Finished crawling</strong>' +
                '</div>';
            $("#crawler").append(alertHTML);
            $("#alertSuccess").show();

            if (extract == false) {
            	var splitted = JSON.stringify(data.values).split(",");
            	
            	var urlLinks = "";         	
            	
            	for (i=0; i<splitted.length; i++) {

	            	if (i == 0) {
	            		
	            		urlLinks = urlLinks + ('<a href=' + splitted[i].substr(1) + 'target="_blank" style="color: black">' + splitted[i].substr(1) + '</a><br>');
	            		
	            	} else {
	            		
	            		urlLinks = urlLinks + ('<a href=' + splitted[i] + 'target="_blank" style="color: black">' + splitted[i] + '</a><br>');
	            	}
          
            	}
            	
            	$("#crawler").append('<pre id="urls" class="pre-scrollable"><code>' + urlLinks + '</code></pre>');
                
            } else {
               // $("#crawler").append('<div class="jsoncontainer"><div id="jjson" class="jjson"></div></div>');
               // $("#jjson").jJsonViewer(JSON.stringify(data.values), {
               //     expanded: true
               // });
               
	            var countHTML = '<div id="resultCounts" class="btn-group" role="group"><button type="button" class="btn btn-default">Total <span class="badge">'+ data.counts.modelsSavedCount +'</span></button>' +
					'<button type="button" class="btn btn-default">Business Model Canvas <span class="badge">' + data.counts.businessModelCanvas + '</span></button>' +
					'<button type="button" class="btn btn-default">Lean Canvas <span class="badge">' + data.counts.leanCanvas + '</span></button>' +
					'</div>';
	            $("#crawler").append(countHTML);
            }

            $(".btn-danger").removeClass("hidden");
            $("#spinnercrawl").addClass("hidden");
            $("#spinnercrawl").removeClass("glyphicon-refresh glyphicon-refresh-animate");
            //$("#crawlFalse").removeClass("glyphicon-refresh glyphicon-refresh-animate");
            //$("#crawlTrue").removeClass("glyphicon-refresh glyphicon-refresh-animate");
            //$("#crawlFalse").addClass("glyphicon-search");
            //$("#crawlTrue").addClass("glyphicon-scissors");
            
            


        },
        error: function(data) {

            var message = data.responseText;
            var alertHTML = '<div id="alertDanger" style="display: none;" class="alert alert-danger alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '<strong>Error in Processing: </strong>' +
                '</div>';
            $("#crawler").append(alertHTML);



            $("#alertDanger").append(message);
            $("#alertDanger").show();

			$(".btn-danger").removeClass("hidden");
            $("#spinnercrawl").addClass("hidden");
            $("#spinnercrawl").removeClass("glyphicon-refresh glyphicon-refresh-animate");
            //$(".btn-primary").removeClass("disabled");
            //$("#crawlFalse").removeClass("glyphicon-refresh glyphicon-refresh-animate");
            //$("#crawlTrue").removeClass("glyphicon-refresh glyphicon-refresh-animate");
            //$("#crawlFalse").addClass("glyphicon-search");
            //$("#crawlTrue").addClass("glyphicon-scissors");
        }

    });

}

function onQualityClick(){
	$("#alertDanger").remove();
    $("#alertSuccess").remove();

 	$(".btn-danger").addClass("hidden");
    $("#spinnerqual").removeClass("hidden");
    $("#spinnerqual").addClass("glyphicon glyphicon-refresh glyphicon-refresh-animate");
   // $("#uploadButton").removeClass("glyphicon-globe");
   // $("#uploadButton").addClass("glyphicon-refresh glyphicon-refresh-animate");

        $.ajax({
            type: 'POST',
            url: '/quality',
            timeout: 500000,
            dataType : 'json',
            success: function(data) {

                var alertHTML = '<div id="alertSuccess" style="display: none;" class="alert alert-success alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Calculation successful:</strong> Quality counts were added to DB' +
                    '</div>';
                $("#preprocessing").append(alertHTML);
                $("#alertSuccess").show();

                $(".btn-danger").removeClass("hidden");
		        $("#spinnerqual").addClass("hidden");
		        $("#spinnerqual").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");


            },
            error: function(data) {

                var message = data.responseText;
                var alertHTML = '<div id="alertDanger" style="display: none;" class="alert alert-danger alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Error in Processing: </strong>' +
                    '</div>';
                $("#preprocessing").append(alertHTML);



                $("#alertDanger").append(message);
                $("#alertDanger").show();
				$(".btn-danger").removeClass("hidden");
		        $("#spinnerqual").addClass("hidden");
		        $("#spinnerqual").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");
            }

        });
   
}

function onStemmingClick(){
	$("#alertDanger").remove();
    $("#alertSuccess").remove();

 	$(".btn-danger").addClass("hidden");
    $("#spinnerqual").removeClass("hidden");
    $("#spinnerqual").addClass("glyphicon glyphicon-refresh glyphicon-refresh-animate");
   // $("#uploadButton").removeClass("glyphicon-globe");
   // $("#uploadButton").addClass("glyphicon-refresh glyphicon-refresh-animate");

        $.ajax({
            type: 'POST',
            url: '/stemming',
            dataType : 'json',
            success: function(data) {

                var alertHTML = '<div id="alertSuccess" style="display: none;" class="alert alert-success alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Calculation successful:</strong> Quality counts were added to DB' +
                    '</div>';
                $("#preprocessing").append(alertHTML);
                $("#alertSuccess").show();

                $(".btn-danger").removeClass("hidden");
		        $("#spinnerqual").addClass("hidden");
		        $("#spinnerqual").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");


            },
            error: function(data) {

                var message = data.responseText;
                var alertHTML = '<div id="alertDanger" style="display: none;" class="alert alert-danger alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Error in Processing: </strong>' +
                    '</div>';
                $("#preprocessing").append(alertHTML);



                $("#alertDanger").append(message);
                $("#alertDanger").show();
				$(".btn-danger").removeClass("hidden");
		        $("#spinnerqual").addClass("hidden");
		        $("#spinnerqual").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");
            }

        });
   
}

function onSynonymsClick(){
	$("#alertDanger").remove();
    $("#alertSuccess").remove();

 	$(".btn-danger").addClass("hidden");
    $("#spinnerqual").removeClass("hidden");
    $("#spinnerqual").addClass("glyphicon glyphicon-refresh glyphicon-refresh-animate");
   // $("#uploadButton").removeClass("glyphicon-globe");
   // $("#uploadButton").addClass("glyphicon-refresh glyphicon-refresh-animate");

        $.ajax({
            type: 'POST',
            url: '/synonyms',
            dataType : 'json',
            success: function(data) {

                var alertHTML = '<div id="alertSuccess" style="display: none;" class="alert alert-success alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Calculation successful:</strong> Quality counts were added to DB' +
                    '</div>';
                $("#preprocessing").append(alertHTML);
                $("#alertSuccess").show();

                $(".btn-danger").removeClass("hidden");
		        $("#spinnerqual").addClass("hidden");
		        $("#spinnerqual").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");


            },
            error: function(data) {

                var message = data.responseText;
                var alertHTML = '<div id="alertDanger" style="display: none;" class="alert alert-danger alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Error in Processing: </strong>' +
                    '</div>';
                $("#preprocessing").append(alertHTML);



                $("#alertDanger").append(message);
                $("#alertDanger").show();
				$(".btn-danger").removeClass("hidden");
		        $("#spinnerqual").addClass("hidden");
		        $("#spinnerqual").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");
            }

        });
   
}

function onStemmingSynonymsClick(){
	$("#alertDanger").remove();
    $("#alertSuccess").remove();

 	$(".btn-danger").addClass("hidden");
    $("#spinnerqual").removeClass("hidden");
    $("#spinnerqual").addClass("glyphicon glyphicon-refresh glyphicon-refresh-animate");
   // $("#uploadButton").removeClass("glyphicon-globe");
   // $("#uploadButton").addClass("glyphicon-refresh glyphicon-refresh-animate");

        $.ajax({
            type: 'POST',
            url: '/stemmingSynonyms',
            dataType : 'json',
            success: function(data) {

                var alertHTML = '<div id="alertSuccess" style="display: none;" class="alert alert-success alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Calculation successful:</strong> Quality counts were added to DB' +
                    '</div>';
                $("#preprocessing").append(alertHTML);
                $("#alertSuccess").show();

                $(".btn-danger").removeClass("hidden");
		        $("#spinnerqual").addClass("hidden");
		        $("#spinnerqual").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");


            },
            error: function(data) {

                var message = data.responseText;
                var alertHTML = '<div id="alertDanger" style="display: none;" class="alert alert-danger alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Error in Processing: </strong>' +
                    '</div>';
                $("#preprocessing").append(alertHTML);



                $("#alertDanger").append(message);
                $("#alertDanger").show();
				$(".btn-danger").removeClass("hidden");
		        $("#spinnerqual").addClass("hidden");
		        $("#spinnerqual").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");
            }

        });
   
}

function onSimilarityClick(){
	
	$("#alertDanger").remove();
    $("#alertSuccess").remove();
    $(".jsoncontainer").remove();
    $("#jjson").remove();

 	$(".btn-danger").addClass("hidden");
    $("#spinnersim").removeClass("hidden");
    $("#spinnersim").addClass("glyphicon glyphicon-refresh glyphicon-refresh-animate");
   // $("#uploadButton").removeClass("glyphicon-globe");
   // $("#uploadButton").addClass("glyphicon-refresh glyphicon-refresh-animate");
   
   var testString = $("#testString").val();
   var testField = $("input[name='sourceField']:checked").val();
   var resultField = $("input[name='resultField']:checked").val();
   var compareMode = $("input[name='compareMode']:checked").val();
   var sameMode = $('#sameModeToggle').prop('checked');

        $.ajax({
            type: 'POST',
            url: '/similarities',
            dataType: 'json',
	    	contentType: 'application/json',
	        data: JSON.stringify( { "testString": testString, "testField": testField, "resultField": resultField, "compareMode": compareMode, "sameMode": sameMode } ),
            success: function(data) {

                var alertHTML = '<div id="alertSuccess" style="display: none;" class="alert alert-success alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Similarity check successful:</strong> See result for details' +
                    '</div>';
                $("#similarities").append(alertHTML);
                $("#alertSuccess").show();
                
                
                $("#similarities").append('<div class="jsoncontainer"><div id="jjson" class="jjson"></div></div>');
                $("#jjson").jJsonViewer(JSON.stringify(data), {
                    expanded: true
                });

                $(".btn-danger").removeClass("hidden");
		        $("#spinnersim").addClass("hidden");
		        $("#spinnersim").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");


            },
            error: function(data) {

                var message = data.responseText;
                var alertHTML = '<div id="alertDanger" style="display: none;" class="alert alert-danger alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Error in Processing: </strong>' +
                    '</div>';
                $("#similarities").append(alertHTML);



                $("#alertDanger").append(message);
                $("#alertDanger").show();
				$(".btn-danger").removeClass("hidden");
		        $("#spinnersim").addClass("hidden");
		        $("#spinnersim").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$(".btn-primary").removeClass("disabled");
		        //$("#uploadButton").removeClass("glyphicon-refresh glyphicon-refresh-animate");
		        //$("#uploadButton").addClass("glyphicon-globe");
            }

        });
	

	
	
}

	function fillDynamicIframe(){
		var frameCryptoURL = document.getElementById("inputCryptoURL").value;
		var newURL = "https://canvanizer.com/canvas/" + frameCryptoURL;
		document.getElementById('canvanizerFrame').src = newURL;
		}
		
		
function onRecommendationClick(){
	var cryptoURL = document.getElementById("inputCryptoURL").value;
	var sameMode = $('#sameModeToggle').prop('checked');
	

	$.ajax({
      url: '/getRecommendation',
      data: {
         cryptoUrl: cryptoURL,
         sameMode: sameMode
      },
      //error: function() {
        // $('#info').html('<p>An error has occurred</p>');
      //},
      dataType: 'json',
      contentType: 'application/json',
      /** success: function(data) {
         var $title = $('<h1>').text(data.talks[0].talk_title);
         var $description = $('<p>').text(data.talks[0].talk_description);
         $('#info')
            .append($title)
            .append($description);
      },*/
      type: 'GET'
   });
}

