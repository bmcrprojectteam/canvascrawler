# Canvas Crawler #
## for Business Model Crunching ##

The Canvas Crawler is the main application for the Business Model Crunching project.
It contains the Website and the Node.js backend coding.


##Features:
- Crawl Bing for new Canvases
- Save Canvases to DB
- Test the Recommendation Engine
- Use the recommendation engine for the BMCr website

## Set-Up ##
1. Click the Deploy to Bluemix button below to import this repository into your running bluemix account.
1. Change the following coding:

```
#!javascript
var username = "xxxxxxx-bluemix";
var password = "xxxxxxxxxx";
```
in beginning of the files:

```
#!javascript

crawler.js [line 5,6]
db.js [line 2,3]
quality.js [line 2,3]
recommender.js [line 2,3]
similarities.js [line 2,3]
```

You can find your data here:

![CloudantDB.png](https://bitbucket.org/repo/7EKLn58/images/952229803-CloudantDB.png)



##Dependencies
To function properly this application needs the real recommendation engine
which uses the python implementation of Gensim as engine.

[Gensim Application Repository](https://bitbucket.org/bmcrprojectteam/gensimapplication)

[![Deploy to Bluemix](https://bluemix.net/deploy/button.png)](http://bit.ly/2pjJNuS)