/*eslint-env node, express, request, cheerio, url*/
//------------------------------------------------------------------------------
// Business Model Crunching main node.js application
//------------------------------------------------------------------------------

// This application uses express as its web server
// for more info, see: http://expressjs.com
var express = require('express');
var bodyParser = require('body-parser');
var crawler = require('./routes/crawler');
var db = require('./routes/db');
var quality = require('./routes/quality');
var similarities = require('./routes/similarities');
var recommender = require('./routes/recommender');

// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
var cfenv = require('cfenv');

// create a new express server
var app = express();

// serve the files out of ./public as our main files
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json()); // for parsing application/json

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();
var host = process.env.VCAP_APP_HOST || 'localhost';
var port = process.env.VCAP_APP_PORT || 3000;

// ---------------- Services --------------------------

app.get('/dbCount', db.getCount());
app.get('/getRecommendation', recommender.getRecommendation());
app.post('/crawl', crawler.crawl());
app.post('/getCanvas', crawler.getCanvas());
app.post('/quality', quality.calculateQuality());
app.post('/stemming', quality.stemming());
app.post('/synonyms', quality.synonyms());
app.post('/stemmingSynonyms', quality.stemmingSynonyms());
app.post('/similarities', similarities.analyseData());




// start server on the specified port and binding host
app.listen(port, host, function() {
  // print a message when the server starts listening
  console.log("server starting on " + appEnv.url);
});
