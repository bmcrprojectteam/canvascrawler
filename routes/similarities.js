/*eslint-env node */
var username = "a52a0b27-971e-4ffb-b3cd-c53ee89fd078-bluemix";
var password = "53d9a0d57a31f9b945d658664c9d4eb142f6be7055c6df1066e43eec7291ffad";
var cloudant = require('cloudant');
var natural = require('natural');
var request = require('request');

var stopwordsFilter = require('node-stopwords-filter');
var filter = new stopwordsFilter();




exports.analyseData = function() {
    return function(req, res) {
        global.gc();
        //analyseString(req, res);
        var testString = req.body.testString;
	    var testField = req.body.testField;
	    var resultField = req.body.resultField;
	    // var testMode = req.body.testMode;
	    var compareMode = req.body.compareMode;
	    var sameMode = req.body.sameMode;
        
        getSimiMeasures(req, res, testString, testField, resultField, compareMode, sameMode, sendSimilarityResponse.bind(this));
    };

};

exports.getSimiMeasuresExport = function(){
	return function(req, res, testString, testField, resultField, compareMode, sameMode, fnFunction, fnSuccessCallback){
		getSimiMeasures(req, res, testString, testField, resultField, compareMode, sameMode, fnFunction, fnSuccessCallback);
	}
}


function getSimiMeasures(req, res, testString, testField, resultField, compareMode, sameMode, fnResponseCallback, fnSuccessCallback) {
    
    switch (compareMode) {
        case 'Native':  
        	var compareKey = 'fields';
        	break;
        case 'No stop words':
			var compareKey = 'noStopWords';
        	// As we want to compare stopword filtered and stemed entries 
			// we need to perform these steps for the input string 
			testString = String(filter.filter(testString));
        	break;
		case 'Synonyms for "no stop words"':
			var compareKey = 'synonyms';
			testString = String(filter.filter(testString));
        	break;
        case 'Synonyms for no stop words':
			var compareKey = 'synonyms';
			testString = String(filter.filter(testString));
        	break; 
        case 'Stemmed words without synonyms':
		    var compareKey = 'stemmedWords';
		    testString = String(filter.filter(testString));
		    testString = natural.PorterStemmer.tokenizeAndStem(testString).join(" ");
        	break;
       	case 'Stemmed words with synonyms':
		    var compareKey = 'stemmedSynonyms';
		   	testString = String(filter.filter(testString));
		    testString = natural.PorterStemmer.tokenizeAndStem(testString).join(" ");
        	break;
    }
    
    var url = "https://gensimbmcr.eu-gb.mybluemix.net/api/getsimi/" + sameMode + "/" + compareKey + "/" + testField + "/" + resultField + "/" + testString;
    
     request(url, function(error, response, body) {
    	//console.log (body);
		if (typeof fnResponseCallback == 'function'){
			fnResponseCallback(res, req, body);
			
		} else {
			if (typeof fnSuccessCallback == 'function'){
				fnSuccessCallback(JSON.parse(body));
			}
    	}
    });
    
	
}

function sendSimilarityResponse(res, req, body){
		var bodyObject = JSON.parse(body);
	    res.setHeader('Content-Type', 'application/json');
   		res.json(bodyObject); 
}



function analyseString(req, res) {

    //this.testString = "Difficulty for repurposing content for target audience";
    //this.testField = "Problem";
    this.testString = req.body.testString;
    this.testField = req.body.testField;
    this.resultField = req.body.resultField;
    this.testMode = req.body.testMode;
    this.compareMode = req.body.compareMode;

    cloudant({
        account: username,
        password: password
    }, function(err, conn) {
        if (err) {
            console.log("Could not initialize connection to Cloudant: " + err);
        } else {
        	
        	var compareMode = '';
        	var db = conn.db.use('businessmodelcatalogue');
        	
        	switch (this.compareMode) {
        		case 'Native':
		            var query = {
		                "selector": {
		                    "modelType": {
		                        "$eq": "leanCanvas"
		                    },
		                    "language": {
		                        "$eq": "english"
		                    },
			                "quality": {
      							"$gt": 0.65
    						}
		                },
		                "limit": 100
		            };    
		            compareMode = 'fields';
        		break;
        		case 'No stop words':
		            var query = {
		                "selector": {
		                    "modelType": {
		                        "$eq": "leanCanvas"
		                    },
		                    "language": {
		                        "$eq": "english"
		                    },
		                    "quality": {
      							"$gt": 0.65
    						},
		                    "noStopWords": {
		                        "$exists": true
		                    } 
		                },
		                "limit": 100
		            };     
		            compareMode = 'noStopWords';
		            	// As we want to compare stopword filtered and stemed entries 
						// we need to perform these steps for the input string 
						this.testString = String(filter.filter(this.testString));
        		break;
				case 'Synonyms for "no stop words"':
		            var query = {
		                "selector": {
		                    "modelType": {
		                        "$eq": "leanCanvas"
		                    },
		                    "language": {
		                        "$eq": "english"
		                    },
		                    "quality": {
      							"$gt": 0.65
    						},		                    
		                    "synonyms": {
		                        "$exists": true
		                    }	 
		                },
		                "limit": 100
		            };   	
		            compareMode = 'synonyms';		            
        		break;        		
        		case 'Stemmed words without synonyms':
		            var query = {
		                "selector": {
		                    "modelType": {
		                        "$eq": "leanCanvas"
		                    },
		                    "language": {
		                        "$eq": "english"
		                    },
		                    "quality": {
      							"$gt": 0.65
    						},
    						"stemmedWords": {
		                        "$exists": true
		                    }
		                },
		                "limit": 100
		            };    
		            compareMode = 'stemmedWords';
		            this.testString = String(filter.filter(this.testString));
		            this.testString = natural.PorterStemmer.tokenizeAndStem(this.testString).join(" ");
        		break;
        		case 'Stemmed words with synonyms':
		            var query = {
		                "selector": {
		                    "modelType": {
		                        "$eq": "leanCanvas"
		                    },
		                    "language": {
		                        "$eq": "english"
		                    },
		                    "quality": {
      							"$gt": 0.65
    						},
    						"stemmedSynonyms": {
		                        "$exists": true
		                    }			                    
		                },
		                "limit": 100
		            };   
		            compareMode = 'stemmedSynonyms';
        		break;
            }
            

            db.find(query, function(err, result) {
                if (err) {
                    sendResponse(res, err);
                } else {
                    //console.log('Found %d documents with language english and canvasType leanCanvas', result.docs.length);
                    
                    var compareResult = compare(result.docs,compareMode,this.testField,this.testString);
                    
                    var idArray = compareResult.idArray;
					var similarityArray = compareResult.similarityArray;						
					var resultsTemp = [];
					for (var i = 0; i < idArray.length; i++) {
					    for(var f in result.docs) {
					    	if( resultsTemp.length > 10){
					    		break;
					    	}
					        if(result.docs[f]._id == idArray[similarityArray[i].index].id ) {
					            resultsTemp.push(result.docs[f]);
					        }
					    }					
					}
					
					var resultArray = [];
					for (var i = 0; i < resultsTemp.length; i++) {
						compareResult = compare(resultsTemp,compareMode,this.resultField,idArray[similarityArray[i].index].text);			
										
					//find the desired result fields based on the similarity array					
					for (var i = 0; i < compareResult.idArray.length; i++) {
												
						var obj = {
	                        id :    compareResult.idArray[compareResult.similarityArray[i].index].id,
	                        "measure":  compareResult.similarityArray[i].measure,
	                        "text": compareResult.idArray[compareResult.similarityArray[i].index].text
                        };                            
                        resultArray.push(obj);                        	
                    	}
                    }
                    
				    resultArray.sort(function(a, b) {
				        //Descending sorting 
				        return parseFloat(b.measure) - parseFloat(a.measure);
				    });                    
					
					// print log
					//console.log(resultArray);

                    //return resulting ids
                    res.setHeader('Content-Type', 'application/json');
                    res.json(resultArray);                    
                    }                
            }.bind(this));
        }
    }.bind(this));





}

function compare(resultDocs,compareMode,compareField,testString){
    this.TfIdf = natural.TfIdf;
    this.tfidf = new this.TfIdf();	
	var compareResult = {};
	var idArray = [];
    for (var i = 0; i < resultDocs.length; i++) {
        //Loop over content
        var resultDoc = resultDocs[i][compareMode];

        // build id array for later comparisons

        // Loop over fields
        if (compareMode == 'fields'){
        	var fields = resultDoc;
        }else{
        	var fields = resultDoc.fields;
        }
        

        for (var j = 0; j < fields.length; j++) {

            if (fields[j].name === compareField) {
            	
            	var totalFieldString;
            	
            	if (this.testMode === "total" ){
                	// Loop over cards
                    var cards = fields[j].cards;
                    for (var k = 0; k < cards.length; k++) {
                        var combinedText = cards[k].title + " " + cards[k].note;
                        var textArray = combinedText.split(" ");

                        // add the created Text array to the document corpus
                        totalFieldString = totalFieldString + " " + textArray;
                    }
                	// Add the current id of the canvas to the array
                    var entry = {}
							 entry.id = resultDocs[i]._id;
							 entry.text = totalFieldString;
							 entry.documentIndex = i;
							 idArray.push(entry);
                	this.tfidf.addDocument(totalFieldString);
            	} else {
            		
            		// Loop over cards
                    var cards = fields[j].cards;
                    for (var k = 0; k < cards.length; k++) {
                        var combinedText = cards[k].title + " " + cards[k].note;
                        var textArray = combinedText.split(" ");

                         
                        // Add the current id of the canvas to the array
                   		var entry = {}
							 entry.id = resultDocs[i]._id;
							 entry.text = textArray;
							 entry.documentIndex = i;
							 idArray.push(entry);

                        // add the created Text array to the document corpus
                        this.tfidf.addDocument(textArray);
                	}
            	}
                    
            }
        }
    }

    // Check the input string against the created document corpus
    if(typeof testString == 'string'){
    	var testStringArray = testString.split(" ");
    }else{    	
    	var testStringArray = testString;
    }
    var similarityArray = [];
    this.tfidf.tfidfs(testStringArray, function(i, measure) {
        var entry = {};
        entry.index = i;
        entry.measure = measure;
        similarityArray.push(entry);
//        console.log("index " + entry.index + " measure " + entry.measure);
    });

    similarityArray.sort(function(a, b) {
        //Descending sorting 
        return parseFloat(b.measure) - parseFloat(a.measure);
    });
	
//	idArray.forEach(function(element) {
//	    console.log(element);
//	});	
	
	compareResult.idArray = idArray;	
	compareResult.similarityArray = similarityArray;	
	
	return compareResult;
}


function sendResponse(res, error) {
    // If an error was encountered return message
    if (error) {
        console.log("We’ve encountered an error: " + error);
        return res.status(500).send(error);

    }

    //console.log(process.memoryUsage());
    global.gc();
    // Send the passed response back to the application
    res.setHeader('Content-Type', 'application/json');
    return res.json({
        "ok": "ok"
    });

}