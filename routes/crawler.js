/*eslint-env node */
var request = require('request');
var cheerio = require('cheerio');
var LanguageDetect = require('languagedetect');
var username = "a52a0b27-971e-4ffb-b3cd-c53ee89fd078-bluemix";
var password = "53d9a0d57a31f9b945d658664c9d4eb142f6be7055c6df1066e43eec7291ffad";
var Cloudant = require('cloudant');
var cloudant = Cloudant({account:username, password:password});
var urlsToCrawl;
var googleSearchUrl = "";
var bingSearchUrl = "";
var returnResult = 0;
var crawledCanvasData = [];
var urlCounter = 0;
var modelsSavedCount = 0;
var leanCanvasSavedCount = 0;
var businessModelCanvasSavedCount = 0;
var callCounter = 200; //How many searchpages should be fetched -> callCounter * 10 = Results 

//--------------Service Calls ---------------------------------------------------------------- 
exports.getCanvas = function() {
    return function(req, res) {
        global.gc();
        var data = req.body;
        var url = req.body.url;
		console.log(process.memoryUsage());
        getCanvasFieldsFromURL(url, res, sendResponse);
        

    };
};

exports.crawl = function() {
    return function(req, res) {
    	global.gc();
        var extract = req.body.extract;
        urlsToCrawl = [];
        urlCounter = 0;
        returnResult = 0;
        modelsSavedCount = 0;
        leanCanvasSavedCount = 0;
        businessModelCanvasSavedCount = 0;
        crawledCanvasData = [];
        console.log(process.memoryUsage());
        googleSearchUrl = "https://www.google.de/search?q=site:https://canvanizer.com/canvas/*";
        bingSearchUrl = "http://www.bing.com/search?q=*+site:https://canvanizer.com/canvas/"
            // Execute the Google/Bing Search requests with a random time delay to avoid getting banned for crawling 


        if (extract == false) {

            for (var j = 0; j < callCounter; j++) {
                (function(j) {
                    setTimeout(function() {
                        //getGoogleSearchLinks(googleSearchUrl, res, sendResponse);
                        getBingSearchLinks(bingSearchUrl, res, sendResponse);
                    }, Math.floor(Math.random() * 1000));
                })(j); //Pass current value into self-executing anonymous function
            }
        } else {
            for (var j = 0; j < callCounter; j++) {
                (function(j) {
                    setTimeout(function() {
                        //getGoogleSearchLinks(googleSearchUrl, res, callCanvasFieldsFromUrls);
                        getBingSearchLinks(bingSearchUrl, res, callCanvasFieldsFromUrls);
                    }, Math.floor(Math.random() * 1000));
                })(j); //Pass current value into self-executing anonymous function
            }
        }
    };
};


//--------------Retrieve the Information of a specific URL from Canvanizer-----------------       
var getCanvasFieldsFromURL = function(url, res, callback) {

    var canvasFields;
    var canvas = {};
    var language;
    var allText;

    request(url, function(error, response, body) {
        if (!error) {
            var $ = cheerio.load(body),
                leanCanvas = $(".lean-canvas"),
                businessModelCanvas = $(".business-model-canvas"),
                title = ($("title").text().replace('- Canvanizer - Canvanizer', '')).trim();
            //--------------Decide which Canvas Model is given-----------------            	
            if (leanCanvas.length > 0) {
                var modelType = "leanCanvas";
                canvasFields = ["Problem", "Solution", "Key Metrics", "Unique Value Proposition",
                    "Unfair Advantage", "Channels", "Customer Segments", "Cost Structure", "Revenue Stream"
                ];
            } else if (businessModelCanvas.length > 0) {
                modelType = "BusinessModelCanvas";
                canvasFields = ["Key Partners", "Key Activities", "Value Proposition",
                    "Customer Relationships", "Customer Segments", "Key Resources", "Channels",
                    "Cost Structure", "Revenue Streams"
                ];
            } else {
                error = "No valid Modeltype.";
                callback(res, null, error);
                return;
            }

            var fields = [];

            //--------------Loop over all Fields and filter the child nodes for text-----------------            	
            for (var i = 0; i < canvasFields.length; i++) {

                var field = {}

                var h6 = $("h6:contains(" + canvasFields[i] + ")");
                //Select sibling/child nodes of this with the css class beginning with note_description
                var id = h6.attr('id');
                var cards = [];

                $("#" + id + " ~ div[id^='block_']").each(function() {
                    $("#" + $(this).attr('id') + " span[id^='note_headline']").each(function() {
                        var card = {}
                        card.title = $(this).text();
                        //Retrieve the additional notes text from description
                        card.note = $("#" + $(this).parent().parent().attr('id') + " p[id^='note_description']").text();
                        
                        // Concatenate all texts for language recognition
                        allText = allText + card.title;
                        allText = allText + card.note;
                        
                        card.color = mapColor($(this).parent().parent().attr('data-color'));
                        cards.push(card);
                    });

                });

                field.name = canvasFields[i];
                field.cards = cards;
                fields.push(field);
            }
			
			//Language recognition
			language = checkLanguage(allText);
			
            canvas.url = url;
            canvas.modelType = modelType;
            canvas.title = title;     	
            canvas.language = language;
            canvas.fields = fields;
            
             $ = ' ';
            
 //--------------Save canvas to Database-----------------                 
			saveToDB(canvas, modelType);
			
            callback(res, canvas, null);
            //--------------Return the found information in json format-----------------           

            console.log("The given Model is a " + modelType + ".");
        } else {
            callback(res, null, error);

        }
    });

};


//--------------Retrieve Google Urls-------------------------------------------------------- 
function getGoogleSearchLinks(url, res, callback) {
    var searchUrls = [];
    request(url, function(error, response, body) {

        if (error) {
            console.log("Couldn’ t get page because of error: " + error);
            return;
        }

        // load the body of the page into Cheerio so we can traverse the DOM
        var $ = cheerio.load(body),
            links = $(".r a");

        //check if we reached the end of search results
        if ($("p:contains('did not match any documents')").length != 0) {
            callback(res, urlsToCrawl);
            return;
        }

        links.each(function(i, link) {
            // get the href attribute of each link
            var urlCanvanizer = $(link).attr("href");

            // strip out unnecessary junk
            urlCanvanizer = urlCanvanizer.replace("/url?q=", "").split("&")[0];

            if (urlCanvanizer.charAt(0) === "/") {
                return;
            }

            searchUrls.push(urlCanvanizer);

        });
        urlsToCrawl = urlsToCrawl.concat(searchUrls);
        

        // After current page set the Url to the next one						
        googleSearchUrl = navigateGooglePageNext(googleSearchUrl, returnResult);
        if (returnResult >= 100) {
            callback(res, urlsToCrawl);
            return;
        }

    });

}

function navigateGooglePageNext(url) {

    var nextPageUrl;

    //Google Search Pages start by no addition and continue in steps by 10.
    if (url.includes("&start=")) {
        url.indexOf("&start=") + 7;
        var currentPageCount = url.substr(url.indexOf("&start=") + 7, url.length);
        var nextPageCount = parseInt(currentPageCount) + 10;
        // Add the next page counter to the existing String	
        nextPageUrl = url.substr(0, url.indexOf("&start=") + 7) + nextPageCount;

    } else {
        // Only add the next page query	
        nextPageUrl = url + "&start=10"

    }
    returnResult = nextPageCount;
    return nextPageUrl;

}

//--------------Retrieve Bing Urls-------------------------------------------------------- 
function getBingSearchLinks(url, res, callback) {
    var searchUrls = [];
    request(url, function(error, response, body) {

        if (error) {
            console.log("Couldn’ t get page because of error: " + error);
            return;
        }

        // load the body of the page into Cheerio so we can traverse the DOM
        var $ = cheerio.load(body),
            links = $(".b_algo h2 a");

        //check if we reached the end of search results
        if ($(".sb_pagN").length == 0) {
        	 if (returnResult >= callCounter * 10 ) {
	        	callback(res, urlsToCrawl);
	            return;
            }
        }

        links.each(function(i, link) {
            // get the href attribute of each link
            var urlCanvanizer = $(link).attr("href");

            // strip out unnecessary junk
            urlCanvanizer = urlCanvanizer.replace("/url?q=", "").split("&")[0];

            if (urlCanvanizer.charAt(0) === "/") {
                return;
            }

            searchUrls.push(urlCanvanizer);

        });
        urlsToCrawl = urlsToCrawl.concat(searchUrls);

        // After current page set the Url to the next one						
        bingSearchUrl = navigateBingPageNext(bingSearchUrl, returnResult);
        if (returnResult >= callCounter * 10 ) {
            callback(res, urlsToCrawl);
            return;
        }

    });

}


function navigateBingPageNext(url) {

    var nextPageUrl;

    //Bing Search Pages start by no addition and continue in steps by 10.
    if (url.includes("&first=")) {
        url.indexOf("&first=") + 7;
        var currentPageCount = url.substr(url.indexOf("&first=") + 7, url.length);
        var nextPageCount = parseInt(currentPageCount) + 10;
        // Add the next page counter to the existing String	
        nextPageUrl = url.substr(0, url.indexOf("&first=") + 7) + nextPageCount;

    } else {
        // Only add the next page query	
        nextPageUrl = url + "&first=11"

    }
    returnResult = nextPageCount;
    return nextPageUrl;

}


//--------------Callback Functions-------------------------------------------------------- 
function sendResponse(res, values, error) {
    // If an error was encountered return message	
    if (error) {
        console.log("We’ve encountered an error: " + error);
        return res.status(500).send(error);

    }

	//Create response counter
	var counts = {};
	counts.leanCanvas = leanCanvasSavedCount; 
	counts.businessModelCanvas = businessModelCanvasSavedCount;
	counts.modelsSavedCount = modelsSavedCount;
	
	console.log(process.memoryUsage());
	global.gc();
    // Send the passed response back to the application	
    res.setHeader('Content-Type', 'application/json');
    return res.json({counts,
        values
    });
	
}

function addToArray(res, values, error) {
    // This function adds retrieved values to the Array crawledCanvasData
    // In case of reached url limit the data is send back to the application
	urlCounter = urlCounter + 1;
		
    if (urlsToCrawl.length <= urlCounter) {
        sendResponse(res, crawledCanvasData);
    } else {
        if (!error) {
            crawledCanvasData = crawledCanvasData.concat(values);
            
        } else {
            return null;
        }
    }

}

function callCanvasFieldsFromUrls(res, urls) {
    // This function calls for every found Url the Method getCanvasFieldsFromURL
    for (var i = 0; i < urls.length; i++) {
        (function(i) {
            setTimeout(function() {
            	//Only get new values if the current Model is not already saved
            	//As this is doen async teh callback function is called within
            	isModelSaved(urls[i], res, getCanvasFieldsFromURL);                	
                
            }, Math.floor(Math.random() * 10000));
        })(i); //Pass current value into self-executing anonymous function
    }


}

//--------------Helper Functions-------------------------------------------------------- 
function mapColor(color) {
    switch (color) {
        case 'rn':
            color = 'red';
            break;
        case 'gn':
            color = 'green';
            break;
        case 'an':
            color = 'blue';
            break;
        case 'yn':
            color = 'yellow';
            break;
        case 'en':
            color = 'grey';
            break;
        default:
            break;
    }
    return color;
}

function increaseUrlCount(res){
	urlCounter = urlCounter + 1;
	if (urlsToCrawl.length <= urlCounter) {
        sendResponse(res, crawledCanvasData);
    } 
}


//--------------Language-Detection--------------------------------------------------------
function checkLanguage(string){
	var lngDetector = new LanguageDetect();
	var language = "";
	
	language = lngDetector.detect(string, 1);
	if(language != "") return language[0][0];
	
 
}

//--------------Database Interface-------------------------------------------------------- 
function isModelSaved(url, res, callback){

	var bmc = cloudant.db.use('businessmodelcatalogue');
	
	bmc.get(url, function(err, data) {
		//Url was not found and can therefore be retrieved from canvanizer
		if (err) {
			callback(url, res, addToArray);
		} else {
			increaseUrlCount(res);
		}

  });

}

function saveToDB(data, modelType){
	
    var bmc = cloudant.db.use('businessmodelcatalogue');
 
    // ...and insert a document in it. 
    bmc.insert( data, data.url, function(err, body, header) {
      if (err) {
        return console.log('[bmc.insert] ', err.message);
      }
      
 	  switch (modelType) {
		case "leanCanvas":
			leanCanvasSavedCount+= 1;
			break;
		case "BusinessModelCanvas":
			businessModelCanvasSavedCount+= 1;
		break;
		}
	
		modelsSavedCount+= 1;
		
      	console.log('Insert complete.');
      	console.log(body);
    });
    

}