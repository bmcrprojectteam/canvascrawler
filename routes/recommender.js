/*eslint-env node */
var username = "a52a0b27-971e-4ffb-b3cd-c53ee89fd078-bluemix";
var password = "53d9a0d57a31f9b945d658664c9d4eb142f6be7055c6df1066e43eec7291ffad";
var cloudant = require('cloudant');
var request = require('request');
var similarities = require('./similarities');
var async = require('async');

exports.startEngine = function () {
    return function (req, res) {
        global.gc();
    };
};


exports.stopEngine = function () {
    return function (req, res) {
        global.gc();
    };
};


exports.getRecommendation = function () {
    return function (req, res) {
        global.gc();
        var cryptoUrl = req.query.cryptoUrl;
        var sameMode = req.query.sameMode;
        retrieveCanvanizerUpdate(cryptoUrl, sameMode);
    };
};


function retrieveCanvanizerUpdate(cryptoUrl, sameMode) {

    var canvanizerUrl = "https://canvanizer.com/plan_updater/update/????????";
    canvanizerUrl = canvanizerUrl.replace("????????", cryptoUrl);

    request(canvanizerUrl, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log(body);

            var canvasRevisionObject = JSON.parse(body);
            var currentRevision = parseInt(canvasRevisionObject.current_revision);
            var lastRevision = currentRevision - 1;

            canvanizerUrl = canvanizerUrl + "/" + parseInt(lastRevision);

            request(canvanizerUrl, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    console.log(body);

                    var canvasObject = JSON.parse(body);

                    var cardArray = canvasObject.detail;

                    // question number = Canvas Feld
                    // note position = Position

                    for (var i = 0; i < cardArray.length; i++) {

                        //new & update betrachten "status"
                        if (cardArray[i].status === 'update' || cardArray[i].status === 'new') {
                            var cardObject = {};
                            cardObject.headline = cardArray[i].headline;
                            cardObject.description = cardArray[i].description;
                            cardObject.totalString = cardArray[i].headline + " " + cardArray[i].description;

                            var testField;
                            var resultField;
                            var resultFieldId;
                            var compareMode = "Stemmed words with synonyms";
                            var skipLoop = false;

                            switch (cardArray[i].question_number) {
                                case "1":
                                    testField = "Problem";
                                    resultField = "Solution";
                                    resultFieldId = 2; //Solution
                                    break;
                                case "2":
                                    testField = "Solution";
                                    resultField = "Problem";
                                    resultFieldId = 1; //Problem
                                    break;
                                case "3":
                                    testField = "Unique Value Proposition";
                                    resultField = "Problem";
                                    resultFieldId = 1; //Problem
                                    break;
                                case "4":
                                    testField = "Unfair Advantage";
                                    resultField = "Problem";
                                    resultFieldId = 1;
                                    skipLoop = true;
                                    break;
                                case "5":
                                    testField = "Customer Segments";
                                    resultField = "Channels";
                                    resultFieldId = 7;
                                    break;
                                case "6":
                                    testField = "Key Metrics";
                                    resultField = "Problem";
                                    resultFieldId = 1;
                                    skipLoop = true;
                                    break;
                                case "7":
                                    testField = "Channels";
                                    resultField = "Customer Segments";
                                    resultFieldId = 5;
                                    break;
                                case "8":
                                    testField = "Cost Structure";
                                    resultField = "Problem";
                                    resultFieldId = 1;
                                    skipLoop = true;
                                    break;
                                case "9":
                                    testField = "Revenue Stream";
                                    resultField = "Problem";
                                    resultFieldId = 1;
                                    skipLoop = true;
                                    break;
                            }

                            if (skipLoop) {
                                continue;
                            }
                            // req, res, testString, testField, resultField, compareMode, fnFunction, fnSuccessCallback
                            similarities.getSimiMeasuresExport()(null, null, cardObject.totalString, testField,
                                resultField, compareMode, sameMode, null,
                                postRecommendation.bind(undefined, cryptoUrl, resultFieldId));
                            //postRecommendation(result,cryptoUrl, resultFieldId);
                        }
                    }
                }
            });
        }
    });

}

function postRecommendation(cryptoUrl, resultFieldId, result) {


    async.forEachSeries(result, function (res, callbackNext) {

        var form = {
            headline: '',
            description: res.textSimilarity,
            q_no: resultFieldId,
            plan_id: cryptoUrl,
            color_hex: 'en',
            note_position: ''
        };

        request.post({
                url: 'https://canvanizer.com/plan_updater/create_note/',
                form: form
            },
            function (err, httpResponse, body) {
                if (err) {
                    console.log("Error: " + err);
                }
                callbackNext();
            });

    });


}	



