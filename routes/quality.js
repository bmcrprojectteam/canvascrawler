/*eslint-env node */
var username = "a52a0b27-971e-4ffb-b3cd-c53ee89fd078-bluemix";
var password = "53d9a0d57a31f9b945d658664c9d4eb142f6be7055c6df1066e43eec7291ffad";
var cloudant = require('cloudant');
var natural  = require('natural');

var stopwordsFilter = require('node-stopwords-filter');
var filter = new stopwordsFilter();
//var defineWord = require('define-word-promise');
var tcom = require('thesaurus-com');
var async = require('async');
var keyFields = ["Problem", "Unique Value Proposition", "Customer Segments"];

exports.calculateQuality = function()  {
	return function(req, res) {
		global.gc();
		calculateQualityInDB(req, res);
	}//;

};

exports.stemming = function()  {
	return function(req, res) {
		global.gc();
		generateStemmedContent(req, res);
	}
};


exports.synonyms = function()  {
	return function(req, res) {
		global.gc();
		//generateSynonyms(req, res); // not used due to latency problems
		generateSynonymsSynchronous(req, res);
	}//;

};

exports.stemmingSynonyms = function()  {
	return function(req, res) {
		global.gc();
		generateStemmedSynonyms(req, res);
	}//;

};


function calculateQualityInDB(req, res){
	cloudant({account: username, password: password}, function (err, conn) {
	  if (err) {
	    console.log("Could not initialize connection to Cloudant: " + err);
	  } else {

	    var db = conn.db.use('businessmodelcatalogue');

	    var query = {   "selector": {
	      "modelType": {
	        "$eq": "leanCanvas"
	      },
	      "language": {
	        "$eq": "english"
	      }
	    }}; // }, "limit":1 };

	    db.find(query, function(err, result) {
	      if (err) {
	        sendResponse(res, err);
	      }
	      else {
	        //console.log('Found %d documents with language english and canvasType leanCanvas', result.docs.length);
	        
	  

	  
	  
	// Loop over document
	        for (var i = 0; i < result.docs.length; i++) {
	          
	          var result2 = JSON.parse(JSON.stringify(result.docs[i]));
	          delete result2._id;
			  delete result2._rev;
	          delete result2.url;
	          delete result2.modelType;
	          delete result2.title;
	          delete result2.language;
	 // if source json already contains a quality we delete the duplicated one
	          if (result2.quality !== null) {
	          	delete result2.quality;
	          }
	
	// delete exiting noStopWords Elements
	          if (result2.noStopWords !== null) {
	          	delete result2.noStopWords;
	          }
	          //TODO rework deleting to futureproof the handling
	           if (result2.stemmedWords !== null) {
	          	delete result2.stemmedWords;
	          }
	          
	          if (result2.synonyms !== null) {
	          	delete result2.synonyms;
	          }
	          
	           if (result2.stemmedSynonyms !== null) {
	          	delete result2.stemmedSynonyms;
	          }
	          
	// Initialize values
	          var filledFields = [0,0,0,0,0,0,0,0,0]; //[Problem, Solution, Key Metrics, Unique Value Proposition, Unfair Advantage, Channels, Customer Segments, Cost Structure, Revenue Stream]
	          var qualityCountSum = 0;
	          var qualityLanguage = 0;
	          var qualityFields = 0;
	          var qualityWords = 0;
	          var quality = 0.00;


	// Loop over fields
	          var fields = result.docs[i].fields;
	          for (var j = 0; j < fields.length; j++){
	// Initialize values
	          var qualityCount = 0;
	          var cardNote = "";
	          var note = "";

	// Loop over cards
	            var cards = fields[j].cards;
	            for (var k = 0; k < cards.length; k++){
	// Concatenate title and note text
	              cardNote = cards[k].title.concat(" ",cards[k].note);
	// Filter out stopWords and save in result2
				  result2.fields[j].cards[k].title = String(filter.filter(cards[k].title));
				  result2.fields[j].cards[k].note = String(filter.filter(cards[k].note));
	            }

	// Concatenate all cards of same field
	            note = note.concat(cardNote);
	// filter Stopwords
	            var filteredNote = String(filter.filter(note));
	// count words
	            var wordCount = countWords(filteredNote);
	// calculate quality
	            if (wordCount <= 1){
	              qualityCount = 1;
	            }
	            else if (wordCount <= 2){
	              qualityCount = 2;
	            }
	            else if (wordCount >= 3){
	              qualityCount = 5;
	            }
	// double points for keyfields
	            if (isInArray(fields[j].name, keyFields)){
	              qualityCount = qualityCount * 2;
	            };
	// add points
	            qualityCountSum = qualityCountSum + qualityCount;

	            // console.log(fields[j].name);
	            //  console.log(wordCount);
	            // console.log(qualityCount);
	            // console.log(qualityCountSum);

	            if (wordCount > 1){
	              filledFields[j] = 1;
	            }

	          }
	// Convert Word Count points
	          if (qualityCountSum <= 20){
	            qualityWords = 1;
	          }
	          else if (qualityCountSum <= 40){
	            qualityWords = 2;
	          }
	          else if (qualityCountSum <= 57){
	            qualityWords = 3;
	          }
	          else if (qualityCountSum > 57){
	            qualityWords = 5;
	          }

	// Calculate Field points
	          var filledFieldsCount = countFieldsFilled(filledFields);
	          // console.log(filledFields);
	          // console.log(filledFieldsCount);

	          if (filledFieldsCount >=5 && filledFieldsCount <= 8 && areKeyFieldsFilled(filledFields)){
	            qualityFields = 4;
	          }
	          else if (filledFieldsCount == 9){
	            qualityFields = 7;
	          }
	          else {
	            qualityFields = 1;
	          }

	// Calculate Language points
	          if (result.docs[i].language == "english"){
	            qualityLanguage = 8;
	          }
	          else {
	            qualityLanguage = 1;
	          }

	// Calculate total quality
	          // console.log(qualityWords);
	          // console.log(qualityFields);
	          // console.log(qualityLanguage);
	          var qualitySum = qualityWords + qualityFields + qualityLanguage;
	          // console.log(qualitySum);
	          quality = qualitySum / 20;
	          // console.log(quality);

	// Update document
	          result.docs[i].quality = quality;
	          result.docs[i].noStopWords = result2;
	          db.insert(result.docs[i], err);

	        }
	       // *Aufgerufen in Stemming Methode  
	        sendResponse(res, null);
	      }
	    });
	  }
	});
}

function countWords(str) {
  if (str == "")
  {
    return 0;
  }
  else {
    return str.split(",").length;
  }
}

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}

function countFieldsFilled(array) {
  var fc = 0;
  for (var x = 0; x < array.length; x++ ){
     fc = fc + array[x];
  }
  return fc;
}

function areKeyFieldsFilled(array) {
  // 0 = Problem
  // 3 = UniqueValueProposition
  // 6 = Customer Segments
  return ((array[0] == 1) && (array[3] == 1) && (array[6] == 1));
}

function sendResponse(res, error) {
    // If an error was encountered return message
    if (error) {
        console.log("We’ve encountered an error: " + error);
        return res.status(500).send(error);

    }

	console.log(process.memoryUsage());
	global.gc();
    // Send the passed response back to the application
    res.setHeader('Content-Type', 'application/json');
    return res.json({"ok" : "ok"});

}




function generateStemmedContent(req, res){
	cloudant({account: username, password: password}, function (err, conn) {
	  if (err) {
	    console.log("Could not initialize connection to Cloudant: " + err);
	  } else {

	    var db = conn.db.use('businessmodelcatalogue');

		var query =   {   "selector": {
					      "modelType": {
					        "$eq": "leanCanvas"
					      },
					      "language": {
					        "$eq": "english"
					      },
					      "noStopWords": {
					      	"$exists" : true
					      },
					      "stemmedWords": {
					      	"$exists": false
					      }
					    }, "limit" : 100 }; 

	    db.find(query, function(err, result) {
	      if (err) {
	        sendResponse(res, err);
	      }
	      else {
	        //console.log('Found %d documents with language english and canvasType leanCanvas', result.docs.length);

	var stemmedWords = {};


	// Loop over document
	        for (var i = 0; i < result.docs.length; i++) {
	//Loop over nostopwords content
			  var noStopWords = result.docs[i].noStopWords;

			  stemmedWords.fields = [];
	// Loop over fields
	          var fields = noStopWords.fields;
	          
	          for (var j = 0; j < fields.length; j++){
	          	
	          	stemmedWords.fields[j] = {};
					          	
	          	stemmedWords.fields[j].name =  fields[j].name;
				
				stemmedWords.fields[j].cards = [];
				
	// Loop over cards
	            var cards = fields[j].cards;
	            for (var k = 0; k < cards.length; k++){
	            	
	            stemmedWords.fields[j].cards[k] = {};
	            
	            stemmedWords.fields[j].cards[k].title = natural.PorterStemmer.tokenizeAndStem(cards[k].title).join(" ");
	            stemmedWords.fields[j].cards[k].note  = natural.PorterStemmer.tokenizeAndStem(cards[k].note).join(" ");	
	            }     

	          }

	// Update document
	          result.docs[i].stemmedWords = stemmedWords;
	          db.insert(result.docs[i],function(err, doc) {
			      if (err) {
			        console.log('Error inserting data\n'+err);
			      }
			
			      console.log('Inserted stemmed words');

			    });

	        }
	        sendResponse(res, null);
	      }
	    });
	  }
	});
}

function generateSynonymsSynchronous(req, res){
	cloudant({account: username, password: password}, function (err, conn) {
	  if (err) {
	    console.log("Could not initialize connection to Cloudant: " + err);
	  } else {

	    var db = conn.db.use('businessmodelcatalogue');

		var query =   {   "selector": {
					      "modelType": {
					        "$eq": "leanCanvas"
					      },
					      "language": {
					        "$eq": "english"
					      },
					      "noStopWords": {
					      	"$exists" : true
					      },
					      "synonyms": {
					      	"$exists": false
					      }
					    }, "limit" : 100 }; 

	    db.find(query, function(err, result) {
	      if (err) {
	        sendResponse(res, err);
	      }
	      else {
	        //console.log('Found %d documents with language english and canvasType leanCanvas', result.docs.length);

	var synonyms = {};


	// Loop over document
	        for (var i = 0; i < result.docs.length; i++) {
	//Loop over nostopwords content
			  var noStopWords = result.docs[i].noStopWords;

			  synonyms.fields = [];
	// Loop over fields
	          var fields = noStopWords.fields;
	          
	          for (var j = 0; j < fields.length; j++){
	          	
	          	synonyms.fields[j] = {};
					          	
	          	synonyms.fields[j].name =  fields[j].name;
				
				synonyms.fields[j].cards = [];
				
	// Loop over cards
	            var cards = fields[j].cards;
	            for (var k = 0; k < cards.length; k++){
	            	
		            synonyms.fields[j].cards[k] = {};
		            
		            // Build title
		            var arrayTitle = [];
					arrayTitle = cards[k].title.split(",");
					var cardTitleResult = '';
					
					for (var l = 0; l < arrayTitle.length; l++) {
						var synonymArray = getSynonymsSync(arrayTitle[l], 2);
						var wordPlusSynonyms = arrayTitle[l] + "," + synonymArray.join(",");
						
						if (cardTitleResult.length == 0){
						  	cardTitleResult+= wordPlusSynonyms;
						}else{
							cardTitleResult+= "," + wordPlusSynonyms;
						}
						
					}
					
					//Build note
					var arrayNote = [];
					arrayNote = cards[k].note.split(",");
					var cardNoteResult = '';
					
					for (var m = 0; m < arrayNote.length; m++) {
						var synonymArray = getSynonymsSync(arrayNote[m], 2);
						var wordPlusSynonyms = arrayNote[m] + "," + synonymArray.join(",");
						
						if (cardNoteResult.length == 0){
					  		cardNoteResult+= wordPlusSynonyms;
						}else{
							cardNoteResult+= "," + wordPlusSynonyms;
						}
						
					}
			
					// Build result
		            synonyms.fields[j].cards[k].title = cardTitleResult;
		            synonyms.fields[j].cards[k].note  = cardNoteResult;	
	            }     
	          }

			// Update document
	          result.docs[i].synonyms = {};
	          result.docs[i].synonyms = synonyms;
	          db.insert(result.docs[i], function(err, doc) {
			      if (err) {
			        console.log('Error inserting data\n'+err);
			      }
			
			      console.log('Inserted synonyms');

			    });

	        }
	        sendResponse(res, null);
	      }
	    });
	  }
	});
}


function getSynonymsSync(word, numberOfSynonyms){
	// get the Synonyms for the word
	// the synonym count can be defined by numberOfSynonyms parameter

 	var thesaurusResult = tcom.search(word);
 	var synonyms = thesaurusResult.synonyms;

	var returnArray = [];
	for (var i = 0; i < numberOfSynonyms; i++) {
		returnArray.push(synonyms[i]);		
	}

	return returnArray;

}		

function generateStemmedSynonyms(req, res){
	cloudant({account: username, password: password}, function (err, conn) {
	  if (err) {
	    console.log("Could not initialize connection to Cloudant: " + err);
	  } else {

	    var db = conn.db.use('businessmodelcatalogue');

		var query =   {   "selector": {
					      "modelType": {
					        "$eq": "leanCanvas"
					      },
					      "language": {
					        "$eq": "english"
					      },
					      "synonyms": {
					      	"$exists" : true
					      },
					      "stemmedSynonyms": {
					      	"$exists": false
					      }
					    }, "limit" : 100 }; 

	    db.find(query, function(err, result) {
	      if (err) {
	        sendResponse(res, err);
	      }
	      else {
	        //console.log('Found %d documents with language english and canvasType leanCanvas', result.docs.length);

	var stemmedSynonyms = {};


	// Loop over document
	        for (var i = 0; i < result.docs.length; i++) {
	//Loop over synonyms content
			  var synonyms = result.docs[i].synonyms;

			  stemmedSynonyms.fields = [];
	// Loop over fields
	          var fields = synonyms.fields;
	          
	          for (var j = 0; j < fields.length; j++){
	          	
	          	stemmedSynonyms.fields[j] = {};
					          	
	          	stemmedSynonyms.fields[j].name =  fields[j].name;
				
				stemmedSynonyms.fields[j].cards = [];
				
	// Loop over cards
	            var cards = fields[j].cards;
	            for (var k = 0; k < cards.length; k++){
	            	
	            stemmedSynonyms.fields[j].cards[k] = {};
	            
	            stemmedSynonyms.fields[j].cards[k].title = natural.PorterStemmer.tokenizeAndStem(cards[k].title).join(" ");
	            stemmedSynonyms.fields[j].cards[k].note  = natural.PorterStemmer.tokenizeAndStem(cards[k].note).join(" ");	
	            }     

	          }

	// Update document
	          result.docs[i].stemmedSynonyms = stemmedSynonyms;
	          db.insert(result.docs[i],function(err, doc){
			      if (err) {
			        return console.log('Error', err.message);
			      }
			
			      console.log('Synonyms were added');
			    });
	        }
	        sendResponse(res, null);
	      }
	    });
	  }
	});
}




/*
 * This function is used to add for every word some other synonyms.
 * This is done through a webservice call to thesaurus by using a npm plugin
 * The noStopwords content is used as base
 */
function generateSynonyms(req, res){
	cloudant({account: username, password: password}, function (err, conn) {
	  if (err) {
	    console.log("Could not initialize connection to Cloudant: " + err);
	  } else {

	    var db = conn.db.use('businessmodelcatalogue');

	    var query =   {   "selector": {
					      "modelType": {
					        "$eq": "leanCanvas"
					      },
					      "language": {
					        "$eq": "english"
					      },
					      "noStopWords": {
					      	"$exists" : true
					      },
					      "synonyms": {
					      	"$exists": false
					      }
					    }, "limit" : 100 }; 

	    db.find(query, function(err, result) {
	      if (err) {
	        sendResponse(res, err);
	      }
	      else {
	        //console.log('Found %d documents with language english and canvasType leanCanvas', result.docs.length);

			var synonyms = {};
			
			// Loop over all documents in the result set
			async.forEachSeries(result.docs, function(doc, callbackDocuments) { 
		        
		        	this.modifiedFields = [];
		        	var noStopWords = doc.noStopWords;
					var fields = noStopWords.fields;
						 
					// Loop over all fields of a single document 
					async.forEachSeries(fields, function(field, callbackFields) { 
						   //call the processing function which will call the callbackFields whenever an iteration is done
						   iterateFields(field, callbackFields); 		        
					    
					}, function(err) {
					    if (err) return err;
						
						// Build the modified document and save it to the database
					    var modifiedDoc = doc;
					   	modifiedDoc.synonyms = {};
					    modifiedDoc.synonyms.fields = this.modifiedFields;
					    // save the modified document to the database
					    db.insert(modifiedDoc, function(err, doc) {
					      if (err) {
					        return console.log('Error', err.message);
					      }
					
					      console.log('Synonyms were added');

					    });
					    if (err) return err;
					    //Call the outer Callback 
					   	callbackDocuments();
					    
					});
		        		        		        
		        
		    }, function(err) {
		        if (err) return err;
		        //As we are in the last method in the chain we can sende the response to the application
		        sendResponse(res, null);
		    });
	      }
	      
	    });
	  }
	});
}


function iterateFields(field, callbackFields){
	var cards = field.cards;
	
	    this.modifiedField = {};
		this.modifiedField.name =  field.name;
		this.modifiedField.cards = [];
		
	//Loop over all cards of a field	
	async.forEachSeries(cards, function(card, callbackCards) { 
	    
	    //call processing function 
	    checkCard(card, callbackCards);
	    		        
	    
	}, function(err) {
	    if (err) return err;
	    //push the modified Field to the fields array (global array)
	    this.modifiedFields.push(this.modifiedField);
	    //call the callback to signal this iteration is done
	   	callbackFields();
	    
	});
	
}

function checkCard(card, callbackCards){
	
   		// for every card we need to build the new title and the note
	    async.series([
	    	function(callbackTitle){
	    		//Retireve the title synonyms (and original words) 
		    	getTitleSynonyms(card, callbackTitle)
		    },
		    function(callbackNote){
		    	// Retrive the note synonyms (and original words)
		    	getNoteSynonyms(card, callbackNote)
		    }
		], function (err, results) {
		    
		    //Build the card content based on the returned data of the serial function calls
		    var modifiedCard = {};
		    modifiedCard.title = results[0]; //The first entry in the result array is the title
		    modifiedCard.note = results[1];	 //The second entry is the note
		    //Add the card to the field array (global array)
		    this.modifiedField.cards.push(modifiedCard);
		    
		    // call the callback to signal we are done
		    callbackCards();
		       
		});
	
	
}

function getTitleSynonyms(card, callbackTitle){
	var arrayTitle = [];
	arrayTitle = card.title.split(",");
	
	var cardTitleResult = '';
						            
	
	async.forEachSeries(arrayTitle, function(word, callbackWords) { 
	    	// Get synonyms for each word cuurently the count is 2
	    async.series([
	    	function(callbackSynonyms){
		    	getSynonyms(word, 2, callbackSynonyms);
		    }
		], function (err, results) {		    
				// build the result string and concatenate the words + synonyms
				var wordPlusSynonyms = word + "," + results[0].join(",");
				if (cardTitleResult.length == 0){
					cardTitleResult+=wordPlusSynonyms;
				}else{	
					cardTitleResult+= "," + wordPlusSynonyms;
				}
				//signal done
				callbackWords();		       
		});	
	    		        
	    
	}, function(err) {
	    if (err) return err;
	    // return the generated title
	   	callbackTitle(null, cardTitleResult);
	    
	});
		
}

function getNoteSynonyms(card, callbackNote){

	var arrayNote = [];
	arrayNote = card.note.split(",");
		
	var cardNoteResult = '';
						            
	
	async.forEachSeries(arrayNote, function(word, callbackWords) { 
	     // Get synonyms for each word cuurently the count is 2
	    async.series([
	    	function(callbackSynonyms){
		    	getSynonyms(word, 2, callbackSynonyms);
		    }
		], function (err, results) {		    
				// build the result string and concatenate the words + synonyms
				var wordPlusSynonyms = word + "," + results[0].join(",");
				if (cardNoteResult.length == 0){
				  cardNoteResult+= wordPlusSynonyms;
				}else{
				cardNoteResult+= "," + wordPlusSynonyms;
				}
				//signal done
				callbackWords();
		});			
	    		        
	    
	}, function(err) {
	    if (err) return err;
	    // return the generated title
	   	callbackNote(null, cardNoteResult);
	    
	});

}

function getSynonyms(word, numberOfSynonyms, callbackSynonyms){
	// get the Synonyms for the word
	// the synonym count can be defined by numberOfSynonyms parameter

 	var thesaurusResult = tcom.search(word);
 	var synonyms = thesaurusResult.synonyms;

	var returnArray = [];
	for (var i = 0; i < numberOfSynonyms; i++) {
		returnArray.push(synonyms[i]);		
	}
	callbackSynonyms(null,returnArray);

}
